package org.cent.example.admin.client.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: cent
 * @email: 292462859@qq.com
 * @date: 2019/2/11.
 * @description:
 */
@RestController
public class HelloController {

    /**
     * 测试接口
     * @return
     */
    @GetMapping("/hello")
    public String hello(){
        return "Hello,Spring Boot Admin!";
    }
}
