package org.cent.example.mqtt.producer;

import org.cent.example.mqtt.producer.service.MqttService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author: cent
 * @email: 292462859@qq.com
 * @date: 2019/2/13.
 * @description:
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MqttServiceTester {

    @Autowired
    private MqttService mqttService;

    @Test
    public void send() {
        int i = 0;
        while (i < 1000) {
            mqttService.send("test-" + i++);
            try {
                Thread.sleep(500l);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
