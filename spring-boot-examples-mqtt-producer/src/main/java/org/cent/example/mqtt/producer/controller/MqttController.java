package org.cent.example.mqtt.producer.controller;

import org.cent.example.mqtt.producer.service.MqttService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: cent
 * @email: 292462859@qq.com
 * @date: 2019/2/13.
 * @description:
 */
@RestController
public class MqttController {

    @Autowired
    private MqttService mqttService;

    @GetMapping("/send")
    public void send(String message) {
        mqttService.send(message);
    }
}
