package org.cent.example.mqtt.producer.service;

/**
 * @author: cent
 * @email: 292462859@qq.com
 * @date: 2019/2/13.
 * @description:
 */
public interface MqttService {

    void send(String message);
}
