package org.cent.example.mqtt.producer.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.cent.example.mqtt.producer.config.prop.MqttProp;
import org.cent.example.mqtt.producer.service.MqttService;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.internal.wire.MqttPersistableWireMessage;
import org.eclipse.paho.client.mqttv3.internal.wire.MqttWireMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author: cent
 * @email: 292462859@qq.com
 * @date: 2019/2/13.
 * @description:
 */
@Service
@Slf4j
public class MqttServiceImpl implements MqttService {

    @Autowired
    private MqttClient mqttClient;
    @Autowired
    private MqttProp mqttProp;

    @Override
    public void send(String message) {
        MqttMessage mqttMessage = new MqttMessage();
        mqttMessage.setQos(1);
        mqttMessage.setPayload(message.getBytes());
        mqttMessage.setRetained(false);

        try {
            mqttClient.publish(mqttProp.getTopic(), mqttMessage);
        } catch (MqttException e) {
            log.error("推送消息失败！", e);
        }
    }
}
