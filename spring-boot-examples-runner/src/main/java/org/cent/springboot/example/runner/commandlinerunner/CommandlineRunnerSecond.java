package org.cent.springboot.example.runner.commandlinerunner;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author: cent
 * @email: 292462859@qq.com
 * @date: 2019/1/29.
 * @description:
 */
@Component
@Slf4j
@Order(1)
public class CommandlineRunnerSecond implements CommandLineRunner {
    @Override
    public void run(String... args) throws Exception {
        log.info("This is {} Command Line Runner", "second");
    }
}
