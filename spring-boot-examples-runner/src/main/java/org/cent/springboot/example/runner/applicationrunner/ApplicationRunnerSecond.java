package org.cent.springboot.example.runner.applicationrunner;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author: cent
 * @email: 292462859@qq.com
 * @date: 2019/1/29.
 * @description:
 */
@Component
@Slf4j
@Order(2)
public class ApplicationRunnerSecond implements ApplicationRunner {
    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("This is {} Application Runner", "second");
    }
}
