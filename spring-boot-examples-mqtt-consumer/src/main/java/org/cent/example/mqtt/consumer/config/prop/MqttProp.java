package org.cent.example.mqtt.consumer.config.prop;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Random;

/**
 * @author: cent
 * @email: 292462859@qq.com
 * @date: 2019/2/13.
 * @description:
 */
@ConfigurationProperties(prefix = "mqtt")
@Data
public class MqttProp {

    private String host = "localhost";

    private Integer port = 1883;

    private String topic = "default-topic";

    private String clientId = "client-" + new Random().ints(100);

    private String username;

    private String password;


}
