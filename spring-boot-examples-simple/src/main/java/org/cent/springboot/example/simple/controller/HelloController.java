package org.cent.springboot.example.simple.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: cent
 * @email: 292462859@qq.com
 * @date: 2019/1/20.
 * @description:
 */
@RestController
public class HelloController {

    /**
     * 示例接口
     *
     * @return
     */
    @GetMapping("/hello")
    public String hello() {
        return "Hello,Spring Boot!";
    }
}
