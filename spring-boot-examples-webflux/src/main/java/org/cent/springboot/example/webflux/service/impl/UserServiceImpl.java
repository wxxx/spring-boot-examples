package org.cent.springboot.example.webflux.service.impl;

import org.cent.springboot.example.webflux.entity.User;
import org.cent.springboot.example.webflux.service.UserService;
import org.springframework.stereotype.Service;

/**
 * @author: cent
 * @email: chenzhao@viomi.com.cn
 * @date: 2019/3/5.
 * @description:
 */
@Service
public class UserServiceImpl implements UserService {
    @Override
    public User findById(String uid) {
        try {
            //模拟数据库查询耗时100ms.
            Thread.sleep(100L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return new User(uid,"cent");
    }
}
