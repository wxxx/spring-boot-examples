package org.cent.springboot.example.webflux.routes;

import org.cent.springboot.example.webflux.handler.UserHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.accept;

/**
 * @author: cent
 * @email: chenzhao@viomi.com.cn
 * @date: 2019/3/5.
 * @description:
 */
@Configuration
public class Routes {

    @Bean
    public RouterFunction userRoutes(UserHandler userHandler) {
        return RouterFunctions
                .route(
                        GET("/user2/{uid}").and(accept(MediaType.APPLICATION_JSON_UTF8)),
                        userHandler::findById);
    }
}
