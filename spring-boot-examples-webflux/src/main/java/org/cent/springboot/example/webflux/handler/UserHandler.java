package org.cent.springboot.example.webflux.handler;

import org.cent.springboot.example.webflux.entity.User;
import org.cent.springboot.example.webflux.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

/**
 * @author: cent
 * @email: chenzhao@viomi.com.cn
 * @date: 2019/3/5.
 * @description:
 */
@Component
public class UserHandler {

    @Autowired
    private UserService userService;

    /**
     * @param request
     * @return
     */
    public Mono<ServerResponse> findById(ServerRequest request) {
        String uid = request.pathVariable("uid");
        return ServerResponse
                .ok()
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(Mono.justOrEmpty(userService.findById(uid)), User.class);
    }
}
