package org.cent.springboot.example.webflux.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author: cent
 * @email: chenzhao@viomi.com.cn
 * @date: 2019/3/5.
 * @description:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {

    private String id;

    private String name;
}
