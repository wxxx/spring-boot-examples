package org.cent.springboot.example.webflux.service;

import org.cent.springboot.example.webflux.entity.User;

/**
 * @author: cent
 * @email: chenzhao@viomi.com.cn
 * @date: 2019/3/5.
 * @description:
 */
public interface UserService {

    /**
     *
     * @param uid
     * @return
     */
    User findById(String uid);
}
