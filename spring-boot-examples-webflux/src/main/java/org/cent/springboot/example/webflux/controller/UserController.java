package org.cent.springboot.example.webflux.controller;

import org.cent.springboot.example.webflux.entity.User;
import org.cent.springboot.example.webflux.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * @author: cent
 * @email: chenzhao@viomi.com.cn
 * @date: 2019/3/5.
 * @description:
 */
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/user1/{uid}")
    public Mono<User> user(@PathVariable String uid) {
        return Mono.justOrEmpty(userService.findById(uid));
    }
}
