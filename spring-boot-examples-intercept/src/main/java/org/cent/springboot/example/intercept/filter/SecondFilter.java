package org.cent.springboot.example.intercept.filter;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

/**
 * @author: cent
 * @email: 292462859@qq.com
 * @date: 2019/2/26.
 * @description: 第二个过滤器
 */
@WebFilter(urlPatterns = "/*")
@Slf4j
public class SecondFilter implements Filter {

    /**
     * @param servletRequest
     * @param servletResponse
     * @param filterChain
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        log.info("[{}]执行{}方法：Before！", this.getClass().getSimpleName(), "doFilter");
        //执行下一个filter
        filterChain.doFilter(servletRequest, servletResponse);
        log.info("[{}]执行{}方法：After！", this.getClass().getSimpleName(), "doFilter");
    }
}
