package org.cent.springboot.example.intercept.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: cent
 * @email: 292462859@qq.com
 * @date: 2019/2/26.
 * @description: 测试请求接口类
 */
@RestController
@Slf4j
public class HelloController {

    /**
     * 测试请求方法
     *
     * @return
     */
    @GetMapping("/hello")
    public String hello() {
        log.info("[{}]执行{}方法！", this.getClass().getSimpleName(), "hello");
        return "Hello!";
    }
}
