package org.cent.springboot.example.intercept;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

/**
 * @author: cent
 * @email: 292462859@qq.com
 * @date: 2019/2/26.
 * @description: 应用启动类
 */
@SpringBootApplication
@ServletComponentScan(basePackages = "org.cent.springboot.example.intercept.filter")
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
