package org.cent.springboot.example.intercept.config;

import org.cent.springboot.example.intercept.interceptor.FirstInterceptor;
import org.cent.springboot.example.intercept.interceptor.SecondInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author: cent
 * @email: 292462859@qq.com
 * @date: 2019/2/26.
 * @description:
 */
@Configuration
public class InterceptorConfig implements WebMvcConfigurer {

    /**
     * 重写添加拦截器方法
     *
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new FirstInterceptor())
                .addPathPatterns("/**")
                .order(1);//指定执行顺序，数值越小越优先
        registry.addInterceptor(new SecondInterceptor())
                .addPathPatterns("/hello")
                .order(2);//指定执行顺序，数值越小越优先
    }

}
