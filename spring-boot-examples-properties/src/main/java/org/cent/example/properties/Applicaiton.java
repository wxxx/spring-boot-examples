package org.cent.example.properties;

import org.cent.example.properties.props.BizProp;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import java.util.Arrays;

/**
 * @author: cent
 * @email: 292462859@qq.com
 * @date: 2019/1/27.
 * @description:
 */
@SpringBootApplication
@EnableConfigurationProperties({BizProp.class})
public class Applicaiton {

    /**
     * 主方法
     *
     * @param args
     */
    public static void main(String[] args) {
        //模拟命令行参数指定profiles
        //args = Arrays.copyOf(args, args.length + 1);
        //args[args.length - 1] = "--spring.config.location=classpath:./custom_location/";

        //启动应用
        SpringApplication.run(Applicaiton.class, args);
    }


}
