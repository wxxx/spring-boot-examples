package org.cent.example.properties.controller;

import org.cent.example.properties.props.BizProp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: cent
 * @email: 292462859@qq.com
 * @date: 2019/1/28.
 * @description:
 */
@RestController
public class DemoController {

    @Autowired
    private BizProp bizProp;

    @GetMapping("/hello")
    public BizProp hello() {
        return bizProp;
    }
}
