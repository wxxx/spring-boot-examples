package org.cent.example.properties.props;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author: cent
 * @email: 292462859@qq.com
 * @date: 2019/1/28.
 * @description:
 */
@ConfigurationProperties(prefix = "org.cent.biz")
public class BizProp {

    private String name;

    private String description;

    public String getName() {
        return name;
    }

    public BizProp setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public BizProp setDescription(String description) {
        this.description = description;
        return this;
    }
}
